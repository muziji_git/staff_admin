import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from "unplugin-vue-components/vite"
import {ElementPlusResolver} from "unplugin-vue-components/resolvers";
import * as path from "path";

// https://vitejs.dev/config/
export default defineConfig({
    resolve:{
        alias:{
            "@":path.resolve(__dirname,"src"),
        }
    },
    // plugins 插件，进行项目构建的时候选择的vue,所以有vue
    // 可以自定义组件，我们需要将element-plus导入到项目中
    plugins: [
        vue(),
        AutoImport({
            resolvers: [ElementPlusResolver()],
        }),
        Components({
            resolvers: [ElementPlusResolver()],
        }),
    ],
})
