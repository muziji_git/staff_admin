import axios from "axios";


axios.defaults.baseURL = "http://127.0.0.1:8000/";

// 请求拦截器，发送请求的时候需要做的事情
axios.interceptors.request.use(config => config);

// 响应拦截器，对返回的数据错误码处理
axios.interceptors.response.use(res => {

    return res
}, error => {
    return Promise.reject(error)
})

export default axios;
