import {createRouter, createWebHashHistory} from "vue-router";

// 路由配置
const routes = [
    {
        path: "/",
        redirect: "login" // 重定向到登录页面
    },
    {
        path: "/login", // 登录页
        name: "login",
        component: () => import("../components/login/index.vue")
    },
    {
        path: "/home",  // 首页布局
        name: "home",
        meta: {title: "首页"},
        component: () => import("../components/home/index.vue"),
        children:[
            {
                path: "/home", // 工单列表（真正的首页）
                name: "detail",
                meta: {title: "状态展示"},
                component: () => import("../components/home/detail/index.vue")
            },
            {
                path: "/staff", // 工单列表
                name: "staff",
                meta: {title: "工单列表"},
                component: () => import("../components/home/staff/index.vue")
            },
            {
                path: "/user",  // 用户管理
                name: "user",
                meta: {title: "用户管理"},
                component: () => import("../components/home/user/index.vue")
            },
            {
                path: "/device",    // 设备管理
                name: "device",
                meta: {title: "设备管理"},
                component: () => import("../components/home/device/index.vue")
            },
            {
                path: "/setting",   //用户信息
                name: "setting",
                meta: {title: "用户信息"},
                component: () => import("../components/home/setting/index.vue")
            },
        ]
    },
]
const router = createRouter({
    history: createWebHashHistory(),
    routes
})
// 导出，如果想让路由被调用，必须要导出
export default router