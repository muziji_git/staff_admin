import {createPinia} from "pinia";
import piniaPluginPersist from "pinia-plugin-persist"

// 创建pina的实例，并导出
const store = createPinia()
store.use(piniaPluginPersist)

export default store
