import {defineStore} from "pinia";

// 用户行为管理
export const useUserStore = defineStore({
    id: "user",
    state: () => {
        return {
            token: "",
            user: "",
            supper: false
        }
    },
    actions: {
        setToken(token, user, supper) {
            this.token = token;
            this.user = user;
            this.supper = supper;
        }
    },
    // 开启数据缓存
    persist: {
        enabled: true,
        strategies: [{
            key: "data_user",
            storage: localStorage,
            paths: ["token", "user", "supper"]
        }]
    }
})
