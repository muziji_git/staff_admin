# 运维管理系统

#### 介绍
学习中...这个网站存在不少的问题，后端接口混乱，前端提交后使用了刷新来重新获取数据，还有其他各种问题...

前后端分离网站，运维管理平台-功能用户管理-设备管理-用户信息-工单管理-统计信息可视化展示

#### 软件架构
软件架构说明


#### 安装教程

1.  安装对应的python环境，安装包
2.  将staff和staff_vite克隆到本地
3.  更改staff中的数据库配置，生成数据库创建管理员账号密码（django rest framework开发）
4.  安装vite项目所需要的包(vite & js）

#### 页面展示

![用户登录页面](https://foruda.gitee.com/images/1693209132881549047/ce934b29_13367372.png "用户登录页面")
![数据统计页面](https://foruda.gitee.com/images/1693209368338283876/724898e9_13367372.png "数据统计页面")
![工单列表页面](https://foruda.gitee.com/images/1693209400967417383/cdaa030e_13367372.png "工单列表页面")
![工单编辑页面](image.png)
![用户列表页面](https://foruda.gitee.com/images/1693209431824436803/8d3cd429_13367372.png "用户列表页面")
![设备管理页面](https://foruda.gitee.com/images/1693209453783075515/cbd68feb_13367372.png "设备管理页面")
![用户信息页面](https://foruda.gitee.com/images/1693209471462651490/8bc5f9e3_13367372.png "用户信息页面")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



