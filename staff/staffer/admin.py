from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from staffer.models import WorkingData, DeviceData, UsersData


@admin.register(WorkingData)
class WorkAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "detail", "staff", "state", "create_at", "update_at", "name", "worker"]
    search_fields = ["title"]
    list_filter = ["state", "title", "name"]


@admin.register(DeviceData)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ["id", "brand", "device_type", "create_at", "update_at", "state", "name", "text"]
    search_fields = ["brand"]
    list_filter = ["state", "name"]


@admin.register(UsersData)
class UserAdministrator(UserAdmin):
    pass
