import base64
import datetime

from django.db.models import Q
from django.contrib.auth.hashers import make_password, check_password
from staffer.models import DeviceData, WorkingData, UsersData
from rest_framework.response import Response
from rest_framework import serializers, status
from rest_framework.authtoken.models import Token
from rest_framework.viewsets import ViewSet
from rest_framework.decorators import authentication_classes, permission_classes, throttle_classes, api_view
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.throttling import AnonRateThrottle
from rest_framework.authentication import BasicAuthentication, TokenAuthentication


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsersData
        fields = "__all__"


class WorkSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source="name.username")
    work = serializers.ReadOnlyField(source="worker.username")
    created = serializers.DateTimeField(source="create_at", read_only=True)
    updated = serializers.DateTimeField(source="update_at", read_only=True)

    class Meta:
        model = WorkingData
        fields = ["id", "title", "detail", "staff", "state", "user", "created", "work", "updated"]


class DeviceSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(source="create_at", read_only=True)
    updated = serializers.DateTimeField(source="update_at", read_only=True)

    class Meta:
        model = DeviceData
        fields = ["id", "brand", "device_type", "name", "state", "text", "created", "updated"]


@api_view(["POST"])
@permission_classes([AllowAny])
def login(request):
    """
    用于判断账号密码是否正确，正确则给他返回token,post请求访问
    :param request:
    :return:
    """
    # 用于判断账号密码是否正确
    if request.method == "POST":
        user = request.data["user"]
        raw_passwd = request.data["password"]
        for i in range(10):
            # 将前端穿回来的加密密码解密
            raw_passwd = base64.b64decode(raw_passwd).decode("utf-8")
        try:
            # 查找数据库中的对应用户名的其他信息，并序列化
            user_data = UserSerializer(UsersData.objects.get(username=user))
        except Exception:
            return Response({"code": 404, "messages": "该用户不存在"}, status=status.HTTP_200_OK)
        if check_password(raw_passwd, user_data.data["password"]):
            try:
                # 根据用户传递回来的信息从数据库获取的其他信息查找赌赢的token,如果存在则输出，不存在则到下方的异常处理中创建
                token = Token.objects.get(user=user_data.data["id"])
                return Response({"code": 200, "token": token.key, "supper": user_data.data["is_superuser"]},
                                status=status.HTTP_200_OK)
            except Exception:
                # 用户存在，但是不存在token，创建一个token
                Token.objects.create(user=UsersData.objects.get(username=user))
                token = Token.objects.get(user=user_data.data["id"])
                return Response({"code": 200, "token": token.key, "supper": user_data.data["is_superuser"]},
                                status=status.HTTP_200_OK)
        return Response({"code": 400, "messages": "用户名或密码错误"}, status=status.HTTP_200_OK)


@authentication_classes([TokenAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
@throttle_classes([AnonRateThrottle])
class HomeData(ViewSet):
    def home_data(self, request):
        """
        前端用于展示首页的统计数据，get请求访问
        :param request:
        :return:
        """
        # 获取获取登录的用户在数据表中的其他信息
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        # 如果数据表中对应的管理员字段为True则执行下面的逻辑，获取所有用户的信息
        lists_seven = []
        lists_month = []
        lists_year = []
        now_time = datetime.datetime.now()
        if user_data.data["is_superuser"]:
            for i in range(7):
                old_time = now_time - datetime.timedelta(days=1)
                work_data = WorkingData.objects.all().filter(Q(create_at__gte=old_time) & Q(create_at__lte=now_time))
                lists_seven.insert(0, work_data.count())
                now_time = old_time
            now_time = datetime.datetime.now()
            for i in range(6):
                old_time = now_time - datetime.timedelta(days=30)
                work_data = WorkingData.objects.all().filter(Q(create_at__gte=old_time) & Q(create_at__lte=now_time))
                lists_month.insert(0, work_data.count())
                now_time = old_time
            now_time = datetime.datetime.now()
            for i in range(12):
                old_time = now_time - datetime.timedelta(days=30)
                work_data = DeviceData.objects.all().filter(Q(create_at__gte=old_time) & Q(create_at__lte=now_time))
                lists_year.insert(0, work_data.count())
                now_time = old_time
            return Response({"code": 200, "seven": lists_seven, "monday": lists_month, "year": lists_year},
                            status=status.HTTP_200_OK)
        for i in range(7):
            old_time = now_time - datetime.timedelta(days=1)
            work_data = WorkingData.objects.all().filter(
                Q(name=self.request.user) & Q(create_at__gte=old_time) & Q(create_at__lte=now_time))
            lists_seven.insert(0, work_data.count())
            now_time = old_time
        now_time = datetime.datetime.now()
        for i in range(6):
            old_time = now_time - datetime.timedelta(days=30)
            work_data = WorkingData.objects.all().filter(
                Q(name=self.request.user) & Q(create_at__gte=old_time) & Q(create_at__lte=now_time))
            lists_month.insert(0, work_data.count())
            now_time = old_time
        lists_year.append("无权访问")
        return Response({"code": 200, "seven": lists_seven, "monday": lists_month, "year": lists_year},
                        status=status.HTTP_200_OK)

    def update_super(self, request):
        """
        用于用户是否管理员的权限修改，put请求访问
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        try:
            # 判断是否有该数据，没有则抛出异常
            user_detail = UsersData.objects.get(pk=self.request.data["id"])
            user_update = UserSerializer(user_detail)
        except Exception:
            return Response({"code": 404, "messages": "没有找到该用户"}, status=status.HTTP_200_OK)
        if user_data.data["is_superuser"] or user_data.data["username"] == str(self.request.user):
            self.request.data["password"] = user_update.data["password"]
            user_data = UserSerializer(instance=user_detail, data=self.request.data)
            if user_data.is_valid():
                user_data.save()
                return Response({"code": 200, "messages": "修改信息成功"}, status=status.HTTP_200_OK)
            return Response(data=user_data.errors, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权查看该用户"}, status=status.HTTP_200_OK)


@authentication_classes([TokenAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
@throttle_classes([AnonRateThrottle])
class WorkGP(ViewSet):

    def home(self, request):
        """
        工单主接口，get请求访问，如果是管理员可以看到全部的工单信息，如果不是只能看到自己的工单信息
        :param request:
        :return:
        """
        # 获取获取登录的用户在数据表中的其他信息
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        # 如果数据表中对应的管理员字段为True则执行下面的逻辑，获取所有用户的信息
        if user_data.data["is_superuser"]:
            work_data = WorkSerializer(instance=WorkingData.objects.all(), many=True)
            return Response(data=work_data.data, status=status.HTTP_200_OK)
        # 如果不是管理员，则获取仅自己的信息
        work_data = WorkSerializer(instance=WorkingData.objects.all().filter(Q(name=self.request.user)), many=True)
        return Response(data=work_data.data, status=status.HTTP_200_OK)

    def staff_add(self, request):
        """
        添加工单信息，post请求访问，所有登录用户都可以向该接口提交工单信息，用户后端自动补全
        :param request:
        :return:
        """
        # 序列化传递过来的数据
        work_data = WorkSerializer(data=self.request.data)
        # 判断数据是否合法
        if work_data.is_valid():
            # 合法则保存，然后创建名字的字段默认是登录用户
            work_data.save(name=self.request.user)
            return Response(data=work_data.data, status=status.HTTP_200_OK)
        return Response(data=work_data.errors, status=status.HTTP_200_OK)


@authentication_classes([BasicAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated])
@throttle_classes([AnonRateThrottle])
class WorkDetail(ViewSet):

    def staff_detail(self, request):
        """
        工单详细接口，post请求访问，管理员可以查看所有的工单详细信息，普通用户只能看到自己的工单详细信，只发送一个指定的数据
        :param request:
        :return:
        """
        # 获取登录用户在数据表中的其他信息
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        try:
            # 如果传递过来的id数据表中没有，这里会报错，捕捉所有的异常
            work_data = WorkSerializer(instance=WorkingData.objects.get(pk=self.request.data["id"]))
        except Exception:
            return Response({"code": 404, "messages": "没有找到该资源"}, status=status.HTTP_200_OK)
        # 如果是管理员和表的所有者，可以查看否则提示无权限
        if user_data.data["is_superuser"] or work_data.data["user"] == str(self.request.user):
            return Response(data=work_data.data, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权查看该资源"}, status=status.HTTP_200_OK)

    def staff_update(self, request):
        """
        工单修改接口，put请求访问，管理员可以修改所有用户，用户仅自己
        :param request:
        :return:
        """
        # 同上
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        try:
            # 获取表中的传递过来的id的数据，如果没有则异常，不序列化，稍后可以在序列化中作为参数直接添加到数据库中
            work_data = WorkingData.objects.get(pk=self.request.data["id"])
            # 获取数据表中的其他参数，并序列化，用于判断用户，不用与数据库存储操作
            user_not_staff = WorkSerializer(instance=WorkingData.objects.get(pk=self.request.data["id"]), many=False)
        except Exception:
            return Response({"code": 404, "messages": "没有找到该资源"}, status=status.HTTP_200_OK)
        # 如果是表的创建者和管理员可以操作，其他用户提示报错，如果是管理员接受工单，则默认将接单用户修改为当前管理员
        if user_data.data["is_superuser"]:
            work_data = WorkSerializer(instance=work_data, data=self.request.data)
            if work_data.is_valid():
                work_data.save(worker=self.request.user)
                return Response(data=work_data.data, status=status.HTTP_200_OK)
            return Response(data=work_data.errors, status=status.HTTP_200_OK)
        if user_not_staff.data["user"] == str(self.request.user):
            work_data = WorkSerializer(instance=work_data, data=self.request.data)
            if work_data.is_valid():
                work_data.save()
                return Response(data=work_data.data, status=status.HTTP_200_OK)
            return Response(data=work_data.errors, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权查看该资源"}, status=status.HTTP_200_OK)

    def staff_delete(self, request):
        """
        工单删除接口，delete请求访问，管理员可以删除所有用户，用户仅自己
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        try:
            # 同上，作为判断用，不参与其他的数据操作
            user_not_staff = WorkSerializer(instance=WorkingData.objects.get(pk=self.request.data["id"]), many=False)
        except Exception:
            return Response({"code": 404, "messages": "没有找到该资源"}, status=status.HTTP_200_OK)
        if user_data.data["is_superuser"] or user_not_staff.data["user"] == str(self.request.user):
            WorkingData.objects.get(pk=self.request.data["id"]).delete()
            return Response({"code": 200, "messages": "已删除该资源"}, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权删除该资源"}, status=status.HTTP_200_OK)


@authentication_classes([BasicAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated])
@throttle_classes([AnonRateThrottle])
class UserList(ViewSet):

    def user_admin(self, request):
        """
        所有用户信息，get请求访问，只有管理员有权限查看，普通用户无权限查看
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        if user_data.data["is_superuser"]:
            user_data = UserSerializer(instance=UsersData.objects.all(), many=True)
            return Response(data=user_data.data, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权查看该资源"}, status=status.HTTP_200_OK)

    def user_add(self, request):
        """
        增加用户，post请求访问，只有管理员有权限添加，普通用户无权添加
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        # 只有管理员可以进行添加操作
        if user_data.data["is_superuser"]:
            user_data = UserSerializer(data=self.request.data)
            try:
                # 如果传递过来的数据中有密码，则进行加密，并替换传递过来的密码，没有则被捕捉异常
                self.request.data["password"] = make_password(self.request.data["password"])
            except KeyError:
                return Response({"code": 400, "password": ["该字段是必填"]}, status=status.HTTP_200_OK)

            if user_data.is_valid():
                user_data.save()
                return Response(data=user_data.data, status=status.HTTP_200_OK)

            return Response({"code": 1000, "errorMsg": user_data.errors}, status=status.HTTP_200_OK)

        return Response({"code": 403, "messages": "无权操作该资源"}, status=status.HTTP_200_OK)


@authentication_classes([BasicAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated])
@throttle_classes([AnonRateThrottle])
class UserDetail(ViewSet):

    def user_detail(self, request):
        """
        查看指定用户，get请求访问，只有管理员有权限查看所有，普通用户仅自己
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        if user_data.data["is_superuser"] or user_data.data["username"] == str(self.request.user):
            return Response(data=user_data.data, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权查看该用户"}, status=status.HTTP_200_OK)

    def user_update(self, request):
        """
        修改指定用户，put请求访问，只有管理员有权限修改所有，普通用户仅自己
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        try:
            # 判断是否有该数据，没有则抛出异常
            user_detail = UsersData.objects.get(pk=self.request.data["id"])
        except Exception:
            return Response({"code": 404, "messages": "没有找到该用户"}, status=status.HTTP_200_OK)

        try:
            raw_passwd = self.request.data["password"]
            for i in range(10):
                # 将前端传回来的加密密码解密
                raw_passwd = base64.b64decode(raw_passwd).decode("utf-8")
            # 对密码进行加密
            self.request.data["password"] = make_password(raw_passwd)
        except KeyError:
            return Response({"code": 201, "password": ["该字段是必填"]}, status=status.HTTP_200_OK)

        if user_data.data["is_superuser"] or user_data.data["username"] == str(self.request.user):
            user_data = UserSerializer(instance=user_detail, data=self.request.data)
            if user_data.is_valid():
                user_data.save()
                try:
                    # 根据用户传递回来的信息从数据库获取的其他信息查找赌赢的token,如果存在则输出，不存在则到下方的异常处理中创建
                    Token.objects.get(user=user_data.data["id"]).delete()
                    Token.objects.create(user=user_detail)
                    token = Token.objects.get(user=user_data.data["id"])
                    return Response({"code": 200, "token": token.key, "supper": user_data.data["is_superuser"]},
                                    status=status.HTTP_200_OK)
                except Exception:
                    # 用户存在，但是不存在token，创建一个token
                    Token.objects.create(user=user_detail)
                    token = Token.objects.get(user=user_data.data["id"])
                return Response({"code": 200, "token": token.key, "supper": user_data.data["is_superuser"]},
                                status=status.HTTP_200_OK)
            return Response(data=user_data.errors, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权查看该用户"}, status=status.HTTP_200_OK)

    def user_delete(self, request):
        """
        查看指定用户，delete请求访问，只有管理员有权限查看所有，普通用户仅自己
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        if user_data.data["is_superuser"]:
            try:
                # 如果传递过来的参数不纯在或者数据库不存在错误，会抛出错误，用于捕捉报错
                UsersData.objects.get(pk=self.request.data["id"]).delete()
                return Response({"code": 200, "messages": "已删除该用户"}, status=status.HTTP_200_OK)
            except Exception:
                return Response({"code": 404, "messages": "没有找到该用户"}, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权删除该用户"}, status=status.HTTP_200_OK)


@authentication_classes([BasicAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated])
@throttle_classes([AnonRateThrottle])
class DeviceList(ViewSet):

    def device_list(self, request):
        """
        查看所有设备，get请求访问，仅管理员有权限查看
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        if user_data.data["is_superuser"]:
            device_data = DeviceSerializer(instance=DeviceData.objects.all(), many=True)
            return Response(data=device_data.data, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权查看该资源"}, status=status.HTTP_200_OK)

    def device_add(self, request):
        """
        添加新的设备，post请求访问，仅管理员有权限查看
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        if user_data.data["is_superuser"]:
            device_data = DeviceSerializer(data=self.request.data)
            if device_data.is_valid():
                device_data.save()
                return Response({"code": 200, "messages": "已更新该设备信息"}, status=status.HTTP_200_OK)
            return Response(data=device_data.errors, status=status.HTTP_200_OK)


@authentication_classes([BasicAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated])
@throttle_classes([AnonRateThrottle])
class DeviceDetail(ViewSet):

    def device_detail(self, request):
        """
        查看指定的设备，post请求访问，仅管理员有权限查看
        :param request:
        :return:
        """
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        if user_data.data["is_superuser"]:
            try:
                device_data = DeviceSerializer(instance=DeviceData.objects.get(pk=self.request.data["id"]))
                return Response(data=device_data.data, status=status.HTTP_200_OK)
            except Exception:
                return Response({"code": 404, "messages": "没有找到该设备"}, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权查看该设备"}, status=status.HTTP_200_OK)

    def device_update(self, request):
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        if user_data.data["is_superuser"]:
            try:
                device_data = DeviceSerializer(instance=DeviceData.objects.get(pk=self.request.data["id"]),
                                               data=self.request.data)
                if device_data.is_valid():
                    device_data.save()
                    return Response({"code": 200, "messages": "已更新该设备信息"}, status=status.HTTP_200_OK)
                return Response({"code": 1000, "errorMsg": device_data.errors}, status=status.HTTP_200_OK)
            except Exception:
                return Response({"code": 404, "messages": "没有找到该设备"}, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权修改该设备"}, status=status.HTTP_200_OK)

    def device_delete(self, request):
        user_data = UserSerializer(instance=UsersData.objects.get(username=self.request.user), many=False)
        if user_data.data["is_superuser"]:
            try:
                DeviceData.objects.get(pk=self.request.data["id"]).delete()
                return Response({"code": 200, "messages": "已删除该设备"}, status=status.HTTP_200_OK)
            except Exception:
                return Response({"code": 404, "messages": "没有找到该设备"}, status=status.HTTP_200_OK)
        return Response({"code": 403, "messages": "无权删除该设备"}, status=status.HTTP_200_OK)
