from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser


class WorkingData(models.Model):
    """
    工单数据表，创建用户和接单用户关联外键表
    """
    title = models.CharField(verbose_name="工单标题", max_length=32)
    detail = models.TextField(verbose_name="工单详情")
    staff = models.TextField(verbose_name="工单处理", null=True, blank=True)
    state_choices = (
        (0, "未受理"),
        (1, "处理中"),
        (2, "未完成"),
        (3, "已完成"),
    )
    state = models.SmallIntegerField(verbose_name="工单状态", choices=state_choices, default=0)
    create_at = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_at = models.DateTimeField(verbose_name="修改时间", auto_now=True)
    name = models.ForeignKey(verbose_name="创建用户", to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                             related_name="user_name")
    worker = models.ForeignKey(verbose_name="接单用户", to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                               related_name="staff_name", null=True, blank=True)

    class Meta:
        verbose_name = "工单数据"
        verbose_name_plural = verbose_name
        ordering = ["state", "-id"]

    def __str__(self):
        return self.title


class DeviceData(models.Model):
    """
    设备数据表
    """
    brand = models.CharField(verbose_name="设备品牌", max_length=32)
    device_type = models.CharField(verbose_name="设备类型", max_length=16)
    create_at = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_at = models.DateTimeField(verbose_name="更新时间", auto_now=True)
    state_choices = (
        (0, "使用中"),
        (1, "闲置中"),
        (2, "已报废"),
    )
    text = models.TextField(verbose_name="设备备注", null=True, blank=True)
    state = models.SmallIntegerField(verbose_name="设备状态", choices=state_choices, default=0)
    name = models.CharField(verbose_name="设备使用人", max_length=32)

    class Meta:
        verbose_name = "设备数据"
        verbose_name_plural = verbose_name
        ordering = ["state", "name"]


class UsersData(AbstractUser):
    """
    用户数据表，继承原来的管理员用户表，在settings.py上添加内容
    """
    ai = models.CharField(verbose_name="个性签名", max_length=64, null=True, blank=True)
