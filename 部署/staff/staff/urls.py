"""
URL configuration for staff project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from staffer import views

urlpatterns = [
    # 管理后台
    path("admin/", admin.site.urls),
    # 登录
    path("apis/login/", views.login),
    # 工单列表和添加
    path("apis/home/", views.WorkGP.as_view({"get": "home", "post": "staff_add"})),
    # 首页数据展示
    path("apis/home_data/", views.HomeData.as_view({"get": "home_data", "put": "update_super"})),
    # 工单查改删
    path("apis/work/",
         views.WorkDetail.as_view({"post": "staff_detail", "put": "staff_update", "delete": "staff_delete"})),
    # 用户列表和添加
    path("apis/user_list/", views.UserList.as_view({"get": "user_admin", "post": "user_add"})),
    # 用户查改删
    path("apis/user_detail/",
         views.UserDetail.as_view({"get": "user_detail", "put": "user_update", "delete": "user_delete"})),
    # 设备列表和添加
    path("apis/device_list/", views.DeviceList.as_view({"get": "device_list", "post": "device_add"})),
    # 设备查改删
    path("apis/device_detail/",
         views.DeviceDetail.as_view({"post": "device_detail", "put": "device_update", "delete": "device_delete"})),
]
